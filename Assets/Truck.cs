﻿using UnityEngine;
using System.Collections;

public class Truck : MonoBehaviour {
    public int a = 0;
    public int b = 0;
    public int c = 0;
    public int d = 0;

    public CommerceLevelManager manager;

	// Use this for initialization
	void Start()
    {
	
	}
	
	// Update is called once per frame
	void Update()
    {
	
	}

    void OnCollisionEnter2D(Collision2D coll)
    {
        Crate crate = coll.collider.GetComponent<Crate>();

        if (crate)
        {
            Destroy(crate.gameObject);

            if      (crate.type == 0) a -= 1;
            else if (crate.type == 1) b -= 1;
            else if (crate.type == 2) c -= 1;
            else if (crate.type == 3) d -= 1;

            if (a < 0 || b < 0 || c < 0 || d < 0) manager.BadLoad();
        }
    }
}
