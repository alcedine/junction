﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class NextLevelScript : MonoBehaviour
{
    public float screenTime;
    public string nextLevelName;
    // Use this for initialization
    void Start()
    {
        screenTime = 5.0f;
    }

    // Update is called once per frame
    void Update()
    {
        screenTime -= Time.deltaTime;
        if (screenTime < 0) SceneManager.LoadScene(nextLevelName);
    }
}
