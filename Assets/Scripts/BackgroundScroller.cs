﻿using UnityEngine;
using System.Collections;

public class BackgroundScroller : MonoBehaviour {
    // Speed at which the background scrolls
    private float scrollSpeed = 0.003f;

	// Use this for initialization
	void Start ()
    {
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        float oldOffset = GetComponent<Renderer>().material.mainTextureOffset.x;
        float offset = oldOffset + scrollSpeed;
        GetComponent<Renderer>().material.mainTextureOffset = new Vector2(offset, 0);
	}
}
