﻿using UnityEngine;
using System.Collections;

public class Pathogen : MonoBehaviour {
    // If not alive, deals no damage
    public bool alive = true; 

    // Size of the play area (maximum movement) in each direction
    private float playAreaRadius = 4.0f;

    private float timeToDie = 0.2f;

    private Rigidbody2D rb2d;

	// Use this for initialization
	void Start ()
    {
        rb2d = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (alive)
        {
            // Walls repel the blood cell
            if (transform.position.y < -playAreaRadius)
            {
                // Wall at -y
                if (rb2d.velocity.y < 0.0)
                {
                    Vector3 v = rb2d.velocity;
                    v.y = -rb2d.velocity.y;
                    rb2d.velocity = v;
                }
            }
            else if (transform.position.y > playAreaRadius)
            {
                // Wall at +y
                if (rb2d.velocity.y > 0.0)
                {
                    Vector3 v = rb2d.velocity;
                    v.y = -rb2d.velocity.y;
                    rb2d.velocity = v;
                }
            }
        }
        else
        {
            timeToDie -= Time.deltaTime;
            if (timeToDie > 0.15f)
            {
                GetComponent<SpriteRenderer>().enabled = false;
            }
            else if (timeToDie > 0.10f)
            {
                GetComponent<SpriteRenderer>().enabled = true;
            }
            else if (timeToDie > 0.05f)
            {
                GetComponent<SpriteRenderer>().enabled = false;
            }
            else if (timeToDie > 0.00f)
            {
                GetComponent<SpriteRenderer>().enabled = true;
            }
            else
            {
                Destroy(gameObject);
            }
        }
	}

    void Die()
    {
        // Already dead?
        if (!alive) return;

        // Die and play pathogen death sound
        alive = false;
        GetComponent<AudioSource>().Play();

        // Tell the level manager that one pathogen has been cured
        LevelHealthtechManager mgr = GameObject.Find("LevelManager").GetComponent<LevelHealthtechManager>();
        mgr.SendMessage("OneCured");
    }
}
