﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class SecondLevel : MonoBehaviour {
    public int countOfCoins;
    public Sprite boxWithCoinSprite;
    public Sprite boxWithoutCoinSprite;
    public Sprite coinSprite;
    public Sprite closedBox;
    public List<Box> boxes = new List<Box>();

	// Use this for initialization
	void Start () {
	}

    // Update is called once per frame
    void Update()
    {
        if (countOfCoins == 3)
        {
            SceneManager.LoadScene("stageFinTechWin");
        }
    }

    public void closeAllBoxes(){
        foreach(Box box in boxes)
        {
            box.sprite.sprite = closedBox; 
        }
        countOfCoins = 0;
    }
}
