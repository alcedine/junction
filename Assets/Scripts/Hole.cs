﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Hole : MonoBehaviour {
    // Is somebody looking out of this hole?
    public bool popped = false;

    // True if the currently popped thing is a robot
    public bool isRobot = false;

    // True if the robot timed out (and killed player)
    public bool isKilling = false;

    // Time until this popped one retreats or kills
    private float timeToLive = 0.0f;

    private GameObject person;
    private GameObject robot;
    private Bounds bounds;

	// Use this for initialization
	void Start ()
    {
        person = transform.FindChild("human").gameObject;
        robot = transform.FindChild("robot").gameObject;

        robot.SetActive(true);
        BoxCollider2D box = robot.GetComponent<BoxCollider2D>();
        bounds = box.bounds;
        Debug.Log(bounds.ToString());
        robot.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (popped)
        {
            timeToLive -= Time.deltaTime;

            if (timeToLive < 0.0f)
            {
                if (isRobot)
                {
                    isKilling = true;
                }
                else
                {
                    popped = false;
                    person.SetActive(false);
                }
            }
        }
	}

    void PopRobot()
    {
        popped = true;
        isRobot = true;
        robot.SetActive(true);
        timeToLive = 2.0f;
    }

    void PopPerson()
    {
        popped = true;
        isRobot = false;
        person.SetActive(true);
        timeToLive = 2.0f;
    }

    void ThwackAll()
    {
        popped = false;
        robot.SetActive(false);
        person.SetActive(false);
    }

    public bool IsHit(Vector2 v)
    {
        Vector3 u = new Vector3(v.x, v.y, 0.0f);
        return bounds.Contains(u);
    }
}
