﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class LevelHealthtechManager : MonoBehaviour {
    public float spawnBloodCellInterval = 1.5f;
    public GameObject bloodCell;

    public float spawnPathogenInterval = 5.0f;
    public GameObject pathogen;

    public int pathogensCured = 0;

	// Use this for initialization
	void Start ()
    {
        InvokeRepeating("SpawnBloodCell", 0.0f, spawnBloodCellInterval);
        InvokeRepeating("SpawnPathogen", 0.0f, spawnPathogenInterval);
	}
	
	// Update is called once per frame
	void Update ()
    {
    }

    void SpawnBloodCell()
    {
        float x = 12.0f + 2.0f * Random.value;
        float y = -3.0f + 6.0f * Random.value;
        Vector3 p = new Vector3(x, y, 0.0f);
        Quaternion r = Quaternion.Euler(0.0f, 0.0f, 360.0f * Random.value);

        GameObject obj = (GameObject)Instantiate(bloodCell, p, r);

        // Randomize velocity
        Rigidbody2D rb = obj.GetComponent<Rigidbody2D>();
        Vector3 v = rb.velocity;
        v.x = -6.0f + 2.0f * (Random.value - 0.5f);
        v.y = 10.0f * (Random.value - 0.5f);
        rb.velocity = v;

        // Randomize rotation speed
        obj.GetComponent<RotateObject>().rotationSpeed = 10.0f * (Random.value - 0.5f);
    }

    void SpawnPathogen()
    {
        float x = 12.0f + 2.0f * Random.value;
        float y = -3.0f + 6.0f * Random.value;
        Vector3 p = new Vector3(x, y, 0.0f);
        Quaternion r = Quaternion.Euler(0.0f, 0.0f, 360.0f * Random.value);

        GameObject obj = (GameObject)Instantiate(pathogen, p, r);

        // Randomize velocity
        Rigidbody2D rb = obj.GetComponent<Rigidbody2D>();
        Vector3 v = rb.velocity;
        v.x = -6.0f + 2.0f * (Random.value - 0.5f);
        v.y = 10.0f * (Random.value - 0.5f);
        rb.velocity = v;

        // Randomize rotation speed
        obj.GetComponent<RotateObject>().rotationSpeed = 10.0f * (Random.value - 0.5f);
    }

    void OneCured()
    {
        pathogensCured += 1;
        GameObject.Find("Text").GetComponent<Text>().text = "Pathogens cured: " + pathogensCured.ToString() + "/3";

        if (pathogensCured >= 3)
        {
            SceneManager.LoadScene("stageHealttechWin");
        }
    }
}
