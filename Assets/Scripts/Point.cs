﻿using UnityEngine;
using System.Collections;

public class Point : MonoBehaviour {
    public bool isConnected;
    public SpriteRenderer pointSprite;
    public Point connectedPoint;
    public Point point;
    public Observer observer;
    public GameObject wire;
    public int id;
    public Material material;

    // Use this for initialization
    void Start () {
        isConnected = false;
        pointSprite = GetComponent<SpriteRenderer>();
        point = GetComponent<Point>();
        wire = new GameObject();
        wire.AddComponent<LineRenderer>();
        

    }
	
	// Update is called once per frame
	void Update () {
        if (isConnected && (connectedPoint != null))
        {
            drawLine(point, connectedPoint);
        }
	}

    void OnMouseDown(){
        if (!isConnected)
        {
            isConnected = true;
            if (observer.selectedPoint != null)
            {
                
                if (observer.checkConnection(point, observer.selectedPoint))
                {
                    connectedPoint = observer.selectedPoint;
                    observer.selectedPoint.connectedPoint = GetComponent<Point>();
                    observer.selectedPoint = null;
                }else
                {
                    print("You Die");
                }

            }
            else
            {
                observer.selectedPoint = GetComponent<Point>();
            }
        }
        pointSprite.color = new Color(255, 0, 0);
    }

    void drawLine(Point firstPoint, Point secondPoint)
    {
        Vector3 firstDot = firstPoint.GetComponent<Transform>().position;
        Vector3 secondDot = secondPoint.GetComponent<Transform>().position;
        Vector3[] dots = { firstDot, secondDot };
        LineRenderer line = wire.GetComponent<LineRenderer>();
        line.SetWidth((float)0.3, (float)0.3);
        line.SetColors(Color.red, Color.red);
        line.materials[0] = material;
        line.SetPositions(dots);
    }
}
