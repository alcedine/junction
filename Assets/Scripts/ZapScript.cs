﻿using UnityEngine;
using System.Collections;

public class ZapScript : MonoBehaviour {
    // How long left to live?
    private float timeToLiveLeft = 0.0f;

    // How long each zap lives
    private float timeToLive = 0.2f;

	// Use this for initialization
	void Start ()
    {
        GameObject.Find("HealthBot").GetComponent<ControlHealthBot>().zap = gameObject;
        gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
    {
        timeToLiveLeft -= Time.deltaTime;

        if (timeToLiveLeft < 0.0f)
        {
            gameObject.SetActive(false);
        }
	}

    void DoZap()
    {
        timeToLiveLeft = timeToLive;
        GetComponent<AudioSource>().Play();
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.gameObject.name == "Pathogen(Clone)")
        {
            c.gameObject.SendMessage("Die");
        }
    }
}
