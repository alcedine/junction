﻿using UnityEngine;
using System.Collections;

public class RotateObject : MonoBehaviour {
    // Speed at which the cell rotates
    public float rotationSpeed = 1.0f;

	// Use this for initialization
	void Start ()
    {
	}
	
	// FixedUpdate is called periodically
	void FixedUpdate ()
    {
        transform.Rotate(0.0f, 0.0f, rotationSpeed);
	}
}
