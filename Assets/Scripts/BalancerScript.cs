﻿using UnityEngine;
using System.Collections;

public class BalancerScript : MonoBehaviour {
    public GameObject balancer;
    public VRController controller;
	// Use this for initialization
	void Start () {   
	}
	
	// Update is called once per frame
	void Update () {
        balancer.transform.position = new Vector3(controller.x + controller.k * controller.angle, controller.y, 0);
	}
}
