﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class KeyBoardController : MonoBehaviour {
    public int[,] keyboardGrid;
    public float x_start;
    public float y_start;
    public float x_delta;
    public float y_delta;
    public List<int> code;
    public Sprite[] numbers = new Sprite[9];
    public GameObject[,] numbersObj = new GameObject[3,3];
    public List<int> rightCode = new List<int>();

    public void fillKeyboardGrid()
    {
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
            {
                keyboardGrid[i, j] = i * 3 + j + 1;
            }
    }

    public void randomizeKeyboard()
    {
        for (int i = 0; i < 100; i++)
        {
            int x_1 = Random.Range(0, 3);
            int x_2 = Random.Range(0, 3);
            int y_1 = Random.Range(0, 3);
            int y_2 = Random.Range(0, 3);
            int buff = keyboardGrid[x_1, y_1];
            keyboardGrid[x_1, y_1] = keyboardGrid[x_2, y_2];
            keyboardGrid[x_2, y_2] = buff;
        }
    }

    // Use this for initialization
    void Start () {
        rightCode.Add(1);
        rightCode.Add(9);
        rightCode.Add(2);
        rightCode.Add(7);
	}

    public bool checkCode()
    {
        if (code.Count == rightCode.Count)
        {
            for (int i = 0; i < rightCode.Count - 1; i++)
                if (code[i] != rightCode[i])
                {
                    SceneManager.LoadScene("stageIntellegenceBuildings");
                }
            return true;
        }
        return false;
    }

    void OnMouseDown()
    {
        //code.Add();
    }
	
	// Update is called once per frame
	void Update () {
        if (checkCode())
        {
            SceneManager.LoadScene("stageIntellegenceBuildingsWin");
        }
	}
}
