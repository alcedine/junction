﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class VRController : MonoBehaviour {
    public float timer = 15.0f;
    public Text timeLeft;
    public float angle;
    public float x;
    public float y;
    public GameObject balancer;
    public GameObject cat;
    public float k;
    // Use this for initialization
    void Start()
    {
        timeLeft = GameObject.Find("TextTimer").GetComponent<Text>();
        angle = 0;
        balancer = GameObject.Find("Balance");
        balancer.transform.position = new Vector3(x, y, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            angle += 0.5f;
            cat.transform.Rotate(new Vector3(0, 0, -0.5f*4));
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            angle -= 0.5f;
            cat.transform.Rotate(new Vector3(0, 0, +0.5f*4));
        }
        timer -= Time.deltaTime;
        if (timer < 5) timeLeft.color = Color.green;
        if (angle > 0)
        {
            float delta = 0.05f + Math.Abs(angle) / 120;
            angle += delta;
            cat.transform.Rotate(new Vector3(0, 0, -delta*4));
        }
        else
        {
            float delta = 0.05f + Math.Abs(angle) / 100;
            angle -= delta;
            cat.transform.Rotate(new Vector3(0, 0, delta*4));
        }
        balancer.transform.position.Set(x + angle * k, y, 0);
        if (System.Math.Abs(angle) >= 7) gameOver();
        if (timer < 0)
        {
            timer = 0;
            winGame();
        }
        timeLeft.text = Math.Round(timer, 2).ToString();
        
	}

    public void gameOver()
    {
        SceneManager.LoadScene("stageVR");
    }
    public void winGame()
    {
        SceneManager.LoadScene("stageVRWin");
    }

}
