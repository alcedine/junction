﻿using UnityEngine;
using System.Collections;

public class BloodCell : MonoBehaviour {
    // Size of the play area (maximum movement) in each direction
    private float playAreaRadius = 4.0f;

    private Rigidbody2D rb2d;

	// Use this for initialization
	void Start ()
    {
        rb2d = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        // Walls repel the blood cell
        if (transform.position.y < -playAreaRadius)
        {
            // Wall at -y
            if (rb2d.velocity.y < 0.0)
            {
                Vector3 v = rb2d.velocity;
                v.y = -rb2d.velocity.y;
                rb2d.velocity = v;
            }
        }
        else if (transform.position.y > playAreaRadius)
        {
            // Wall at +y
            if (rb2d.velocity.y > 0.0)
            {
                Vector3 v = rb2d.velocity;
                v.y = -rb2d.velocity.y;
                rb2d.velocity = v;
            }
        }
	}
}
