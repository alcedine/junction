﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class AILevelManager : MonoBehaviour {
    private bool alive = true;
    private float restartTimeout = 4.0f;

    // How many robots has the player whacked?
    private int robotsShutdown = 0;

    private bool hammerIsDown = false;

    // How many seconds until somebody pops out of a hole?
    private float timeToNextPop = 2.0f;

    // Probability of getting a robot on the pop
    public float probabilityRobot = 0.5f;

    private Hole[] holes;

    private Camera camera;
    private Text statusText;
    private Image hammer;
    private AudioSource thwack;

    private AudioSource killedByRobotSound;
    private AudioSource robotDieSound;
    private AudioSource humanOwSound;

	// Use this for initialization
	void Start ()
    {
        // Cache some stuff
        camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        statusText = GameObject.Find("StatusText").GetComponent<Text>();
        hammer = GameObject.Find("HammerImage").GetComponent<Image>();
        thwack = GameObject.Find("HammerImage").GetComponent<AudioSource>();
        killedByRobotSound = GameObject.Find("LaserSFX").GetComponent<AudioSource>();
        robotDieSound = GameObject.Find("RobotDieSFX").GetComponent<AudioSource>();
        humanOwSound = GameObject.Find("HumanOwSFX").GetComponent<AudioSource>();

        // Initialize the list of holes
        holes = new Hole[8];
        for (int i = 0; i < 8; ++i)
        {
            holes[i] = GameObject.Find("Hole" + (i + 1).ToString()).GetComponent<Hole>();
        }
	}

    // Update is called once per frame
    void Update()
    {
        if (alive) UpdateAlive();
        else UpdateDying();
    }

    void UpdateAlive ()
    {
        // Check for victory condition
        if (robotsShutdown == 10) SceneManager.LoadScene("stageAIWin");

        // Update the status line
        statusText.text = "Robots shut down: " + robotsShutdown.ToString() + "/10";

        // Read user inputs -- where is the mouse?
        Vector2 focus = camera.ScreenPointToRay(Input.mousePosition).origin;
        hammer.transform.position = Input.mousePosition;

        // Read user inputs -- hammer up/down?
        if (hammerIsDown)
        {
            // Hammer is down; we can raise it
            if (Input.GetMouseButtonUp(0))
            {
                hammerIsDown = false;
            }
        }
        else
        {
            // Hammer is up; we can bring it down
            if (Input.GetMouseButtonDown(0))
            {
                hammerIsDown = true;
                thwack.Play();

                for (int i = 0; i < holes.Length; ++i)
                {
                    Hole hole = holes[i];
                    if (hole.IsHit(focus))
                    {
                        if (hole.popped)
                        {
                            if (!hole.isRobot)
                            {
                                alive = false;
                                statusText.text = "Oh no, you killed a human! Should have used the Voigt-Kampff.";
                                humanOwSound.Play();
                            }
                            else
                            {
                                robotsShutdown += 1;
                                robotDieSound.Play();
                            }

                            hole.SendMessage("ThwackAll");
                        }
                    }
                }
            }
        }


        // Update the hammer state (angled downwards if down)
        if (hammerIsDown)
        {
            hammer.transform.rotation = Quaternion.Euler(0, 0, 75);
        }
        else
        {
            hammer.transform.rotation = Quaternion.Euler(0, 0, 0);
        }

        // Pop up robots/people, if necessary
        timeToNextPop -= Time.deltaTime;

        if (timeToNextPop < 0.0f)
        {
            int whichHole = Random.Range(0, holes.Length);
            if (whichHole == holes.Length) whichHole = holes.Length - 1;

            Hole hole = holes[whichHole];
            if (!hole.popped)
            {
                bool popIsRobot = Random.value < probabilityRobot;
                if (popIsRobot) hole.SendMessage("PopRobot");
                else hole.SendMessage("PopPerson");
            }

            timeToNextPop = 0.3f + 0.7f * Random.value;
        }

        // Check that the robot hasn't killed us
        for (int i = 0; i < holes.Length; ++i)
        {
            Hole hole = holes[i];
            if (hole.isKilling)
            {
                alive = false;
                statusText.text = "Too slow! You were killed by a robot!";
                killedByRobotSound.Play();
            }
        }
	}

    void UpdateDying()
    {
        // When dying, wait for a while so the player can read what went wrong
        // (But don't spawn any more people during that time)
        restartTimeout -= Time.deltaTime;

        if (restartTimeout < 0.0f)
        {
            SceneManager.LoadScene("stageAI");
        }
    }
}
