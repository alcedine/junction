﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ControlHealthBot : MonoBehaviour {
    // Zap object
    public GameObject zap;

    // How much does the nose of the bot go up or down when moving?
    private float rotationConstant = 10.0f;

    // Size of the play area (maximum movement) in each direction
    private float playAreaRadius = 3.0f;

    // Maximum speed at which the bot moves up and down
    private float movementRate = 40.0f;

    // Force with which the wall repels the bot
    private float wallSpringConstant = 100.0f;

    // If collides with a bad guy, dies
    private bool alive = true;

    // How much time until allowed to zap again
    private float zapTimeout = 0.0f;

    // The time added to zapTimeout when a zap is executed
    private float zapTimeoutTime = 0.5f;

    // Timeout to restart the level
    private float restartTimeout = 2.0f;

    private Rigidbody2D rb2d;

	// Use this for initialization
	void Start ()
    {
        rb2d = GetComponent<Rigidbody2D>();
        GetComponent<Animator>().speed = 0.5f;
	
	}

    // Called at a fixed time interval
    void FixedUpdate()
    {
        zapTimeout -= Time.deltaTime;

        if (alive)
        {
            // Zapping?
            if (Input.GetKeyDown(KeyCode.Space) && zapTimeout <= 0.0f)
            {
                zapTimeout = zapTimeoutTime;
                zap.SetActive(true);
                zap.SendMessage("DoZap");
            }

            // What movement (if any) is the player requesting?
            float yMove = Input.GetAxis("Vertical");

            // Set the rotation of the sprite
            float targetRotation = rotationConstant * yMove;
            transform.rotation = Quaternion.Euler(0, 0, targetRotation);

            // Update the position of the bot
            Vector2 movementForce = new Vector2(0, movementRate * yMove);
            rb2d.AddForce(movementForce);

            // Walls repel the bot
            if (transform.position.y < -playAreaRadius)
            {
                // Wall at -y
                float excess = (-playAreaRadius) - transform.position.y;
                Vector2 wallSpringForce = new Vector2(0, wallSpringConstant * excess);
                rb2d.AddForce(wallSpringForce);
            }
            else if (transform.position.y > playAreaRadius)
            {
                // Wall at +y
                float excess = transform.position.y - playAreaRadius;
                Vector2 wallSpringForce = new Vector2(0, -wallSpringConstant * excess);
                rb2d.AddForce(wallSpringForce);
            }
        }
        else
        {
            // If dead, spin out
            transform.Rotate(0, 0, 20);

            restartTimeout -= Time.deltaTime;
            if (restartTimeout < 0.0f)
            {
                SceneManager.LoadScene("stageHealthtech");
            }
        }
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        // Only 1st collision matters
        if (!alive) return;

        // Dead pathogens don't kill
        if (c.name == "Pathogen" && !c.GetComponent<Pathogen>().alive) return;

        // Either a blood cell or a pathogen; either way, die
        alive = false;

        rb2d.drag = 0.0f;
        rb2d.velocity = new Vector3(15.0f, 4.0f, 0.0f);

        AudioSource audio = GameObject.Find("Main Camera").GetComponent<AudioSource>();
        audio.loop = false;
        audio.clip = (AudioClip)Resources.Load("SFX/Die");
        audio.Play();
    }
}
