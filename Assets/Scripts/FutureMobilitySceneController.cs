﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class FutureMobilitySceneController : MonoBehaviour {
    public int[,] grid;
    public Sprite[] cars = new Sprite[15];
    public GameObject[] carsObj = new GameObject[15]; 
    public Vector3[,] positions;

    public float x_size;
    public float y_size;
    public float start_x = -3.0f;
    public float start_y = -3.6f;
         
	// Use this for initialization
    public void randomizeLevel()
    {
        for (int i = 0; i < 2000; i++)
        {
            int x = Random.Range(0, 4);
            switch (x)
            {
                case 0:
                    moveDown();
                    break;
                case 1:
                    moveUp();
                    break;
                case 2:
                    moveRight();
                    break;
                case 3:
                    moveLeft();
                    break;
            }
        }
    }

    public void moveDown()
    {
        int[] freePos = freeSpace();
        if (freePos[0] > 0)
        {
            int buff = grid[freePos[0], freePos[1]];
            grid[freePos[0], freePos[1]] = grid[freePos[0] - 1, freePos[1]];
            grid[freePos[0] - 1, freePos[1]] = buff;
        }
    }
    public void moveUp()
    {
        int[] freePos = freeSpace();
        if (freePos[0] < 3)
        {
            int buff = grid[freePos[0], freePos[1]];
            grid[freePos[0], freePos[1]] = grid[freePos[0] + 1, freePos[1]];
            grid[freePos[0] + 1, freePos[1]] = buff;
        }
    }
    public void moveRight()
    {
        int[] freePos = freeSpace();
        if (freePos[1] > 0)
        {
            int buff = grid[freePos[0], freePos[1]];
            grid[freePos[0], freePos[1]] = grid[freePos[0], freePos[1] - 1];
            grid[freePos[0], freePos[1] - 1] = buff;
        }
    }
    public void moveLeft()
    {
        int[] freePos = freeSpace();
        if (freePos[1] < 3)
        {
            int buff = grid[freePos[0], freePos[1]];
            grid[freePos[0], freePos[1]] = grid[freePos[0], freePos[1] + 1];
            grid[freePos[0], freePos[1] + 1] = buff;
        }
    }

    public bool levelCompletionCheck()
    {
        for(int i = 0; i < grid.GetLength(0); i++)
        {
            for(int j = 0; j < grid.GetLength(1); j++)
            {
                if (i == 3 && j == 3) return true;
                if (grid[i, j] != i * 4 + j) return false;
            }
        }
        return true;
    }

    void drawCar(int i, int j, int id)
    {
        if (id == -1) return;
        carsObj[id].transform.position = positions[i, j];
    }

    int[] freeSpace()
    {
        for (int i = 0; i < grid.GetLength(0); i++)
        {
            for (int j = 0; j < grid.GetLength(1); j++)
            {
                if (grid[i, j] == -1)
                {
                    int[] res = { i, j };
                    return res;
                }
            }
        }
        int[] noRes = { -1, -1 };
        return noRes;
    }

	void Start () {
        grid = new int[4, 4];
        positions = new Vector3[4, 4];
        for (int i = 0; i < grid.GetLength(0); i++)
        {
            for (int j = 0; j < grid.GetLength(1); j++)
            {
                grid[i, j] = i * 4 + j;
                Vector3 pos = new Vector3(start_x + x_size * j, start_y - y_size * i, 1);
                positions[i, j] = pos;
            }
        }
        grid[3, 3] = -1;
        for (int i = 0; i < cars.Length; i++)
        {
            carsObj[i] = new GameObject();
            carsObj[i].AddComponent<SpriteRenderer>();
            SpriteRenderer spriteRenderer = carsObj[i].GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = cars[i];
        }
        randomizeLevel();
    }
	
	// Update is called once per frame
	void Update () {
        for (int i = 0; i < grid.GetLength(0); i++)
        {
            for (int j = 0; j < grid.GetLength(1); j++)
            {
                drawCar(i, j, grid[i,j]);
            }
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            moveDown();
        }   
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            moveUp();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            moveRight();
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            moveLeft();
        }

        if (levelCompletionCheck())
        {
            SceneManager.LoadScene("stageFutureMobilityWin");
        }
    }
    
}
