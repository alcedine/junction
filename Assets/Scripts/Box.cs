﻿using UnityEngine;
using System.Collections;

public class Box : MonoBehaviour {
    public bool coinInside;
    public SpriteRenderer sprite;
    public SecondLevel secondLevel;

	// Use this for initialization
	void Start () {
        sprite = GetComponent<SpriteRenderer>();
        secondLevel.boxes.Add(GetComponent<Box>());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseDown()
    {
        if (coinInside)
        {
            sprite.sprite = secondLevel.boxWithCoinSprite;
            secondLevel.countOfCoins++;
        }else
        {
            secondLevel.closeAllBoxes();
        }
    }
}
