﻿using UnityEngine;
using System.Collections;

public class Crane : MonoBehaviour {
    public GameObject hook;
    public GameObject attached;

    private Rigidbody2D rb2d_crane;
    private Rigidbody2D rb2d_hook;

    private CommerceLevelManager manager;

    private float xMotion = 10.0f;
    private float yMotion = 10.0f;
    private float pullForce = 100.0f;

    private bool keyDown = false;

	// Use this for initialization
	void Start()
    {
        rb2d_crane = GetComponent<Rigidbody2D>();
        hook = transform.FindChild("Hook").gameObject;
        rb2d_hook = hook.GetComponent<Rigidbody2D>();

        manager = GameObject.Find("LevelManager").GetComponent<CommerceLevelManager>();
	}
	
	// Update is called once per frame
	void Update()
    {
        float xMove = Input.GetAxis("Horizontal");
        float yMove = Input.GetAxis("Vertical");

        rb2d_crane.AddForce(new Vector2(xMotion * xMove, 0.0f));
        rb2d_hook.AddForce(new Vector2(0.0f, yMotion * yMove));

        Vector2 hookPos = hook.transform.position;
        hookPos.x = transform.position.x;
        hook.transform.position = hookPos;

        if (keyDown)
        {
            if (Input.GetKeyUp(KeyCode.Space))
            {
                keyDown = false;

            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                keyDown = true;

                if (attached)
                {
                    Debug.Log("Released crate");
                    attached = null;
                }
                else
                {
                    attached = manager.GetClosestCrate();
                    if (attached) Debug.Log("Grabbed crate");
                    else Debug.Log("Crates too far");
                }
            }
        }

        // Make sure the attached crate, if any, stays close
        if (attached)
        {
            Vector3 dp = hook.transform.position - attached.transform.position;
            float dist = dp.magnitude;
            if (dist > 1.2f)
            {
                float overflow = dist - 1.2f;
                dp = overflow * dp.normalized;
                attached.GetComponent<Rigidbody2D>().AddForce(pullForce * dp);
            }
        }
	}
}
