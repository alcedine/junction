﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Observer : MonoBehaviour {
    public Point selectedPoint;
    private bool firstConnection;
    private bool secondConnection;
	// Use this for initialization
	void Start () {
        firstConnection = false;
        secondConnection = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (firstConnection && secondConnection)
        {
            SceneManager.LoadScene("stageIOTWin");
        }
    }

    public bool checkConnection(Point first, Point second)
    {
        if ((first.id == 1 && second.id == 4) || (first.id == 4 && second.id == 1))
        {
            firstConnection = true;
            return true;
        }
        if ((first.id == 2 && second.id == 3) || (first.id == 3 && second.id == 2))
        {
            secondConnection = true;
            return true;
        }
        return false;
    }
}
