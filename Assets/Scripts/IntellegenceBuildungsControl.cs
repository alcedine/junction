﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class IntellegenceBuildungsControl : MonoBehaviour {
    public float timer = 15.0f;
    public Text timeLeft;
    // Use this for initialization
	void Start () {
	    timeLeft = GameObject.Find("TextTimer").GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        timer -= Time.deltaTime;
        if (timer < 5) timeLeft.color = Color.red;
        timeLeft.text = System.Math.Round(timer, 2).ToString();
        if (timer < 0)
        {
            SceneManager.LoadScene("stageIntellegenceBuildings");
        }


	}
}
