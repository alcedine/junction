﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class CommerceLevelManager : MonoBehaviour {
    private bool alive = true;
    public GameObject truckType;

    public GameObject hook;
    public GameObject truck1;
    public GameObject truck2;

    public Sprite spriteA;
    public Sprite spriteB;
    public Sprite spriteC;
    public Sprite spriteD;

    public Text truck1Text;
    public Text truck2Text;

    private GameObject[] crates;

    private Text timeLeftLabel;

    private float timeLeft = 90.0f;

    private int trucksLoaded = 0;

    private int cratesA = 0;
    private int cratesB = 0;
    private int cratesC = 0;
    private int cratesD = 0;

	// Use this for initialization
	void Start()
    {
        timeLeftLabel = GameObject.Find("TimerText").GetComponent<Text>();

        crates = new GameObject[10];

        for (int i = 0; i < 10; ++i)
        {
            string crateStr = "Crate" + (i + 1).ToString();
            crates[i] = GameObject.Find(crateStr);

            float whichCrate = Random.Range(0.0f, 4.0f);
            if (whichCrate < 1.0f)
            {
                crates[i].GetComponent<Crate>().type = 0;
                cratesA += 1;
                crates[i].GetComponent<SpriteRenderer>().sprite = spriteA;
            }
            else if (whichCrate < 2.0f)
            {
                crates[i].GetComponent<Crate>().type = 1;
                cratesB += 1;
                crates[i].GetComponent<SpriteRenderer>().sprite = spriteB;
            }
            else if (whichCrate < 3.0f)
            {
                crates[i].GetComponent<Crate>().type = 2;
                cratesC += 1;
                crates[i].GetComponent<SpriteRenderer>().sprite = spriteC;
            }
            else
            {
                crates[i].GetComponent<Crate>().type = 3;
                cratesD += 1;
                crates[i].GetComponent<SpriteRenderer>().sprite = spriteD;
            }
        }
	}

    // Update is called once per frame
    void Update()
    {
        if (alive) UpdateAlive();
        else UpdateDead();
    }

    void UpdateAlive()
    {
        timeLeft -= Time.deltaTime;

        // Check loss condition
        if (timeLeft < 0.0f)
        {
            timeLeft = 4.0f;
            timeLeftLabel.color = Color.red;
            timeLeftLabel.text = "0.0";
            alive = false;
            return;
        }

        // Check win condition -- 3 successful loads
        if (trucksLoaded == 3)
        {
            SceneManager.LoadScene("stageCommerceWin");
        }

        // Trucks done?
        if (truck1.activeSelf)
        {
            Truck truck = truck1.GetComponent<Truck>();
            if (truck.a == 0 && truck.b == 0 && truck.c == 0 && truck.d == 0)
            {
                truck1.SetActive(false);
                trucksLoaded += 1;
            }
        }
        if (truck2.activeSelf)
        {
            Truck truck = truck2.GetComponent<Truck>();
            if (truck.a == 0 && truck.b == 0 && truck.c == 0 && truck.d == 0)
            {
                truck2.SetActive(false);
                trucksLoaded += 1;
            }
        }

        // Need to bring in new trucks?
        if (!truck1.activeSelf)
        {
            MakeTruckInPos(1);
        }

        if (!truck2.activeSelf)
        {
            MakeTruckInPos(2);
        }

        Truck t1 = truck1.GetComponent<Truck>();
        string t1str = t1.a.ToString() + " Apples\n"
            + t1.b.ToString() + " Beer\n"
            + t1.c.ToString() + " Cakes\n"
            + t1.d.ToString() + " Donuts\n";
        truck1Text.text = t1str;

        Truck t2 = truck2.GetComponent<Truck>();
        string t2str = t2.a.ToString() + " Apples\n"
            + t2.b.ToString() + " Beer\n"
            + t2.c.ToString() + " Cakes\n"
            + t2.d.ToString() + " Donuts\n";
        truck2Text.text = t2str;

        // Draw time left
        timeLeftLabel.text = timeLeft.ToString();
	}

    void UpdateDead()
    {
        timeLeft -= Time.deltaTime;

        // Check loss condition
        if (timeLeft < 0.0f) SceneManager.LoadScene("stageCommerce");
    }

    // Returns the crate closest to the hook, if any
    public GameObject GetClosestCrate()
    {
        float best = 10000.0f;
        GameObject bestCrate = null;

        for (int i = 0; i < 10; ++i)
        {
            GameObject crate = crates[i];
            if (!crate) continue;

            float dist = (crate.transform.position - hook.transform.position).magnitude;

            if (dist < best)
            {
                best = dist;
                bestCrate = crate;
            }
        }

        if (best < 1.2f) return bestCrate;
        else return null;
    }

    // This is called when a crate is loaded to the wrong truck
    public void BadLoad()
    {
        alive = false;
        timeLeft = 4.0f;
        timeLeftLabel.color = Color.red;
        timeLeftLabel.text = "WRONG!";
    }

    private void MakeTruckInPos(int pos)
    {
        int a = 0;
        int b = 0;
        int c = 0;
        int d = 0;

        int n = 0;
        while (n < 2)
        {
            float whichCrate = Random.Range(0.0f, 4.0f);
            if (whichCrate < 1.0f)
            {
                if (cratesA == 0) continue;
                a += 1;
                n += 1;
                cratesA -= 1;
            }
            else if (whichCrate < 2.0f)
            {
                if (cratesB == 0) continue;
                b += 1;
                n += 1;
                cratesB -= 1;
            }
            else if (whichCrate < 3.0f)
            {
                if (cratesC == 0) continue;
                c += 1;
                n += 1;
                cratesC -= 1;
            }
            else
            {
                if (cratesD == 0) continue;
                d += 1;
                n += 1;
                cratesD -= 1;
            }
        }

        if (pos == 1)
        {
            Truck truck = truck1.GetComponent<Truck>();
            truck.a = a;
            truck.b = b;
            truck.c = c;
            truck.d = d;
            truck1.SetActive(true);
        }
        else
        {
            Truck truck = truck2.GetComponent<Truck>();
            truck.a = a;
            truck.b = b;
            truck.c = c;
            truck.d = d;
            truck2.SetActive(true);
        }
    }
}
